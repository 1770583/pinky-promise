import Player1 from "./Player1.js"
import Player2 from "./Player2.js";
import Monstre from "./Monstre.js";

export default class Jeu {

    constructor() {
        this.canvas = document.querySelector('canvas');
        this.chargeur = new createjs.LoadQueue();
        this.fCharger();
    }

    fCharger(){
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.addEventListener("complete", this.fInitialiser.bind(this));
        this.chargeur.addEventListener("error", console.log("Erreur de chargement"));
        this.chargeur.loadManifest("ressources/manifest.json");
    }

    fInitialiser() {
        this.canvas.width = 1280;
        this.canvas.height = 800;

        this.stage = new createjs.StageGL(this.canvas);
        this.stage.setClearColor("black");

        createjs.Ticker.framerate = 60;
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;


        createjs.Ticker.addEventListener("tick", e => this.stage.update(e));

        this.fDebuter();

        createjs.Ticker.addEventListener("tick", this.detecter.bind(this));

        createjs.Sound.play("FastAce", {"loop":-1, "volume": 0.05});

    }


    fDebuter() {
        //----------------------Background------------------------------------

        this.fond = new createjs.Bitmap(this.chargeur.getResult("background"));
        this.stage.addChild(this.fond);

        this.fond.x = this.stage.canvas.width / 2 - this.fond.getBounds().width / 2;
        this.fond.y = this.stage.canvas.height/ 2 - this.fond.getBounds().height/ 2;
        //------------------------Player1-------------------------------------------

        this.player1 = new Player1(this.chargeur);
        this.stage.addChild(this.player1);

        this.player1.x = this.stage.canvas.width/2 - this.player1.getBounds().width;
        this.player1.y = this.stage.canvas.height/2 - this.player1.getBounds().height;

        //------------------------Player2-------------------------------------------

        this.Joueur2 = new Player2(this.chargeur);
        this.stage.addChild(this.Joueur2);

        this.Joueur2.x = this.stage.canvas.width / 2;
        console.log(this.Joueur2.x);
        this.Joueur2.y = this.Joueur2.getBounds().height*2;

        // //--------------------------------Pointage----------------------------------

        this.pointage = new createjs.Text(10000, "48px \"Arial\"", "green");
        this.pointage.cache(0, 0, this.pointage.getBounds().width, this.pointage.getBounds().height);
        this.pointage.x = this.stage.canvas.width / 1.5 - this.pointage.getBounds().width / 1.5;
        this.pointage.y = 5;
        this.stage.addChild(this.pointage);
    }

    detecter() {
        let ListeMonstres = this.stage.children.filter(item => item instanceof Monstre);
        let joueur1 = this.stage.children.filter(item => item instanceof Player1);

        ListeMonstres.forEach(monstre => {

            joueur1.forEach(player1 => {

                let p = player1.globalToLocal(monstre.x, monstre.y);

                if (player1.hitTest(p.x, p.y)) {

                    player1.verifAlignement(player1,monstre,this);

                }
            });
        });
    }
    ajusterPointage(e){
        this.pointage.text -= e;
        this.pointage.updateCache();
    }

    finJeu(e){
        let ListeMonstres = this.stage.children.filter(item => item instanceof Monstre);
        ListeMonstres.forEach(monstre => {
            monstre.parent.removeChild(this);
        });
        this.Joueur2.parent.removeChild(this.Joueur2);

        let affichage = "Rien à afficher";
        console.log(localStorage.length+" "+e);

        if(localStorage.length === 0){
            let resume = {
                joueur: 'Martine',
                points: this.pointage
            };

            JSON.stringify(resume);

        } else {
            if(this.pointage > localStorage.key(1)){
                localStorage.clear();
                let resume = {
                    joueur: 'Martine',
                    points: this.pointage
                };

                JSON.stringify(resume);
                localStorage.setItem('Martine', );
            }
        }

        this.score = new createjs.Text(affichage, "48px \"Arial\"", "green");
        this.score.cache(0, 0, this.score.getBounds().width, this.score.getBounds().height);
        this.score.x = this.stage.canvas.width / 1.5 - this.score.getBounds().width / 1.5;
        this.score.y = 5;
        this.stage.addChild(this.score);


    }
}





































//
//
// import Player1 from "./Player1.js"
// import Player2 from "./Player2.js";
//
// export default class Jeu {
//
//     constructor() {
//
//         this.canvas = document.querySelector('canvas');
//         this.chargeur= new createjs.LoadQueue();
//
//         this.fCharger();
//
//     }
//
//     fCharger(){
//
//         this.chargeur.installPlugin(createjs.Sound);
//         this.chargeur.addEventListener("complete", this.fInitialiser.bind(this));
//         this.chargeur.addEventListener("error", console.log("Erreur de chargement"));
//         this.chargeur.loadManifest("ressources/manifest.json");
//
//     }
//
//     fInitialiser() {
//         this.canvas.width =  window.innerWidth;
//         this.canvas.height = window.innerHeight;
//
//         this.stage = new createjs.StageGL(this.canvas);
//         this.stage.setClearColor("black");
//
//         createjs.Ticker.framerate = 60;
//         createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
//
//
//         createjs.Ticker.addEventListener("tick", e => this.stage.update(e));
//
//         this.fDebuter();
//
//         // createjs.Ticker.addEventListener("tick", this.detecter.bind(this));
//     }
//
//
//     fDebuter() {
//         //----------------------Background------------------------------------
//
//         this.fond = new createjs.Bitmap(this.chargeur.getResult("background"));
//         this.stage.addChild(this.fond);
//
//         this.fond.x = this.stage.canvas.width / 2 - this.fond.getBounds().width / 2;
//         this.fond.y = this.stage.canvas.height/ 2 - this.fond.getBounds().height/ 2;
//
//         //------------------------Player1-------------------------------------------
//
//
//
//         //------------------------Player2-------------------------------------------
//
//         this.Joueur2 = new Player2(this.chargeur);
//         this.stage.addChild(this.Joueur2);
//
//         this.Joueur2.x = this.stage.canvas.width / 2 - this.Joueur2.getBounds().width / 2;
//         this.Joueur2.y = this.Joueur2.getBounds().height;
//
//         // //--------------------------------Pointage----------------------------------
//         // this.pointage = new createjs.Text(10000, "48px \"Arial\"", "green");
//         // this.pointage.cache(0, 0, this.pointage.getBounds().width, this.pointage.getBounds().height);
//         // this.pointage.x = this.stage.canvas.width/2 - this.pointage.getBounds().width/2;
//         // this.pointage.y = 50;
//         // this.stage.addChild(this.pointage)
//     }
//
//     // detecter() {
//     //     let listeProjectiles = this.stage.children.filter(elementSurScene => elementSurScene instanceof Projectile);
//     //     let listeblocs = this.stage.children.filter(item => item instanceof Bloc);
//     //     let defense = this.stage.children.filter(item => item instanceof Defenseur);
//     //
//     //
//     //     listeProjectiles.forEach(projectile => {
//     //         listeblocs.forEach(bloc => {
//     //             let position = bloc.globalToLocal(projectile.x, projectile.y);
//     //
//     //             if (bloc.hitTest(position.x, position.y)) {
//     //                 console.log("Ça marche tu caliss?");
//     //
//     //                 this.ajusterPointage(1000);
//     //
//     //                 bloc.detruire();
//     //                 projectile.detruire();
//     //             }
//     //         });
//     //
//     //         listeProjectiles.forEach(projectile => {
//     //             defense.forEach(defense => {
//     //                 let position = defense.globalToLocal(projectile.x, projectile.y);
//     //
//     //                 if (defense.hitTest(position.x, position.y)) {
//     //                     console.log("Ok google!");
//     //
//     //                     this.ajusterPointage(-200);
//     //                     projectile.detruire();
//     //                 }
//     //             });
//     //
//     //
//     //         })
//     //     });
//     // }
//
//     // ajusterPointage(e){
//     //     this.pointage.text += e;
//     //     this.pointage.updateCache();
//     // }
// }
//
