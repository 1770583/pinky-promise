export default class Monstre extends createjs.Sprite {

    constructor(chargeur,leQuel) {
        super(chargeur.getResult("texture"));

        this.leQuel = leQuel;

        this.initialiser();
    }

    initialiser(){

        this.scaleX = 2;
        this.scaleY = 2;

        if(this.leQuel ===0){
            this.gotoAndStop("monstre/Up/Up");
        } else if(this.leQuel ===1){
            this.gotoAndStop("monstre/Right/Right");
        }else if(this.leQuel ===2){
            this.gotoAndStop("monstre/Down/Down");
        } else {
            this.gotoAndStop("monstre/Left/Left");
        }

        createjs.Ticker.addEventListener("tick", e => {
            if(this.leQuel ===0){
                this.y+=5;
            } else if(this.leQuel ===1){
                this.x-=5;
            }else if(this.leQuel ===2){
                this.y-=5;
            } else {
                this.x+=5;
            }});
    }

    detruire(){
        this.parent.removeChild(this);
    }
}