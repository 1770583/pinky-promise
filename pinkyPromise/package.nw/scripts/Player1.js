export default class Player1 extends createjs.Sprite {

    constructor(leChargeur) {
        super(leChargeur.getResult("texture"));
        this.nombre = 0;
        this.score = 0;

        this.scaleX = 4;
        this.scaleY = 4;
        this.setBounds(0, 0, 20,20);

        window.addEventListener('keydown',this.verifToucher.bind(this));
    }

    verifToucher(e) {
        if(e.key === "q"){
            if(this.nombre%2 === 0){
                this.gotoAndStop(9);
            }else{
                this.gotoAndStop(0);
            }
        }

        if(e.key === "w"){
            if(this.nombre%2 === 0){
                this.gotoAndStop(10)
            } else {
                this.gotoAndStop(11)
            }
        }
        this.nombre++;
    }
    verifAlignement(p,m,t){
        console.log('Verif en cours');
        if(p.currentFrame === 9 && m.currentFrame === 7){
            this.confirmation(m,t);
            console.log("Block");
        } else if(p.currentFrame === 0 && m.currentFrame === 1){
            this.confirmation(m,t);
            console.log("Block");
        } else if(p.currentFrame === 10 && m.currentFrame === 5){
            this.confirmation(m,t);
            console.log("Block");
        } else if(p.currentFrame === 11 && m.currentFrame === 3){
            this.confirmation(m,t);
            console.log("Block");
        } else {
            console.log("hit "+t.score);
            m.detruire();
            t.ajusterPointage(250);
            if(t.score = 0){
                t.finJeu(1);
            }
            let random = Math.floor(Math.random()*4+1);
            createjs.Sound.play("hit"+random, {"volume": 0.1});
        }
    }

    confirmation(m,t){
        t.ajusterPointage(-250);
        m.detruire();
        createjs.Sound.play("Shield", {"volume": 0.1});
        this.score++;

        if(this.score === 5){
            t.finJeu(0);
        }
    }
}