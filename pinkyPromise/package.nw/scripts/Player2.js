import Monstre from "./Monstre.js";

export default class Player2 extends createjs.Shape{

    constructor(chargeur) {
        super();
        this.choix = 0;
        this.chargeur = chargeur;

        this.initialiser();
    }

    initialiser() {

        this.graphics
            .setStrokeStyle(5, "round")
            .beginStroke("red")
            .beginFill("red")
            .moveTo(0, 0)
            .lineTo(25, -50)
            .lineTo(-25, -50)
            .lineTo(0, 0);

        this.cache(-50, -50, 100, 100);
        this.setBounds(25, 50, 50, 50);

        window.addEventListener("keydown", this.fTouch.bind(this));
    }

    fTouch(e) {

        this.optionsX = [
            this.stage.canvas.width / 2,
            this.stage.canvas.width - this.getBounds().width*2,
            this.stage.canvas.width / 2,
            this.getBounds().width*2
        ];

        this.optionsY = [
            this.getBounds().height*2,
            this.stage.canvas.height / 2,
            this.stage.canvas.height - this.getBounds().height*1,
            this.stage.canvas.height / 2
        ];

        if (e.key === "e") {
            this.choix++;
            this.fBouger();
        }
        if (e.key ==="r"){
            this.fSpawn();
        }
    }

    fBouger(){
        this.x = this.optionsX[this.choix%4];
        this.y = this.optionsY[this.choix%4];
    }

    fSpawn() {
        const monstre = new Monstre(this.chargeur,this.choix%4);
        monstre.x = this.optionsX[this.choix%4]-monstre.getBounds().width;
        monstre.y = this.optionsY[this.choix%4];
        this.stage.addChild(monstre);
    }
}